from random import randint


class Building:
    """Conception of real building with persons which waiting for elevator for move to the desired floor"""

    def __init__(self):
        self.height = randint(5, 20)
        self.persons_on_floors = {
            i: randint(0, 10)
            for i in range(self.height)
        }
        self.destination_floors = dict()
        for floor, persons_on_floor in enumerate(self.persons_on_floors):
            self.destination_floors[floor] = list()
            for _ in range(persons_on_floor):
                destination = self.random_destination(floor)
                self.destination_floors[floor].append(destination)

    def random_destination(self, current_floor: int) -> int:
        destination = current_floor
        while destination == current_floor:
            destination = randint(1, self.height - 1)
        return destination


